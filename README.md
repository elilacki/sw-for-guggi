# SW-for-Guggi



## Summary

Our project is a sophisticated IoT (Internet of Things) setup that utilizes an ESP32 microcontroller as the central unit. This microcontroller is interfaced with a BME280 sensor, a TFT display, and an LED strip. The BME280 sensor, known for its ability to measure temperature, humidity, and atmospheric pressure, gathers environmental data which is then displayed on the TFT screen. This display is not just numerical; it includes graphical representations of the sensor readings, making it user-friendly and visually appealing.

In addition to the display, the LED strip provides a quick, color-coded temperature indicator: green for normal temperatures, blue for cold, and red for hot conditions. This allows for an immediate visual understanding of the temperature range.

Furthermore, the ESP32 sends the collected data to an MQTT broker. MQTT (Message Queuing Telemetry Transport) is a lightweight messaging protocol, ideal for IoT applications due to its low power and bandwidth requirements.

From the MQTT broker, the data is fetched by a Node-RED application. Node-RED is a programming tool for wiring together hardware devices, APIs, and online services in new and interesting ways. In your project, Node-RED plays a crucial role by retrieving data from the MQTT broker and then storing it in a MySQL database. MySQL serves as a reliable and scalable database management system for storing your sensor data.

Lastly, the stored data is accessed by Grafana, an open-source platform for monitoring and observability. Grafana pulls data from the MySQL database and creates detailed, beautiful visualizations. These visualizations are not only useful for real-time monitoring but also for analyzing trends and patterns over time. This comprehensive setup from sensor to visualization offers a robust solution for environmental monitoring and data analysis.

## Topology

![Screenshot_2024-01-25_212526](/uploads/a8071cf04e487018cf197291af5586c8/Screenshot_2024-01-25_212526.png)

## Our Tools

Docker, Grafana, VS code, Node-red, MySQL, MQTTBox

## Storing (Node-red)

![Screenshot_2024-01-25_212357](/uploads/9de73565e3159afe83e83ffc0df33d73/Screenshot_2024-01-25_212357.png)

## Visualization (Grafana)

![Screenshot_2024-01-25_215628](/uploads/b41689e2a5d40a6a1378e5a520cba02a/Screenshot_2024-01-25_215628.png)



