#include <Arduino.h>

#include <Arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library 
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735 
#include <Adafruit_ST7789.h> // Hardware-specific library for ST7789 
#include <SPI.h> 
#include <Adafruit_BME280.h>
#include <Adafruit_NeoPixel.h>
#include <WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>

//MQTT
const char* ssid = "htl-IoT";
const char* password = "iot..2015";
const char* mqtt_server = "broker.hivemq.com";

#define OUT_TOPIC "hallo/temp01/temp" 
#define OUT_TOPIC1 "hallo/temp01/humi" 
#define IN_TOPIC "hallo/temp01/" 

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE	(50)
char msg[MSG_BUFFER_SIZE];
int value = 0;


#define NTP_OFFSET  7200 // In seconds ZeitZone
#define NTP_INTERVAL 60 * 1000    // In miliseconds = 60 Minuten, 60 Sekunden
#define NTP_ADDRESS  "0.at.pool.ntp.org"

WiFiUDP ntpUDP;

NTPClient timeClient(ntpUDP, NTP_ADDRESS, NTP_OFFSET, NTP_INTERVAL);

#define TFT_CS         16  //Geändert  
#define TFT_RST        5   // Or set to -1 and connect to Arduino RESET pin //Geändert 
#define TFT_DC         17  // Geändert 

Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST); 

//BME

#define BME_SCK 22
#define BME_MISO
#define BME_MOSI
#define BME_CS 21

Adafruit_BME280 bme;

#define SEALEVELPRESSURE_HPA (1013.25)

//Neopixel

Adafruit_NeoPixel strip = Adafruit_NeoPixel(18, GPIO_NUM_32, NEO_RGB + NEO_KHZ800);


void tftPrintTextAndRead() {
  //SWS text
  tft.setTextWrap(false); 
  tft.fillScreen(ST77XX_BLACK); 
  tft.setTextColor(ST77XX_BLUE);
  tft.setCursor(17,5);
  tft.setTextSize(2);
  tft.println("Smart-WS");
  tft.drawLine(tft.width()-1, 21, 0, 21,ST77XX_WHITE);

  //Parameter
  double temp = bme.readTemperature();
  double humi = bme.readHumidity();
  double pres = bme.readPressure();
  double alti = bme.readAltitude(SEALEVELPRESSURE_HPA);

  //Clock
  timeClient.update();
  String formattedTime = timeClient.getFormattedTime();
  // Serial.println(formattedTime);
  tft.setTextColor(ST77XX_WHITE);
  tft.setTextSize(2);
  tft.setCursor(16, 135);
  tft.println(formattedTime);   // write the buffer to the display
  tft.drawLine(tft.width()-1, 128, 0, 128,ST77XX_WHITE);

  //TEMP Text
  if(temp < 22.00) {
    tft.setTextColor(ST77XX_BLUE);
    tft.setCursor(5,24);
    tft.setTextSize(1);
    tft.println("Temperatur: ");
    tft.drawFastVLine(tft.width()/2,70,53,ST77XX_BLUE);
    tft.drawFastHLine(39,96,53,ST77XX_BLUE);
    tft.drawLine(41, 87, 53, 96,ST77XX_BLUE);
    tft.drawLine(41, 105, 53, 96,ST77XX_BLUE);
    tft.drawLine(89, 105, 76, 96,ST77XX_BLUE);
    tft.drawLine(89, 87, 76, 96,ST77XX_BLUE);
    tft.drawLine(tft.width()/2, 85, 55, 72,ST77XX_BLUE);
    tft.drawLine(tft.width()/2, 85, 73, 72,ST77XX_BLUE);
    tft.drawLine(tft.width()/2, 106, 55, 120,ST77XX_BLUE);
    tft.drawLine(tft.width()/2, 106, 73, 120,ST77XX_BLUE);


  }else if(temp > 25.00) {
    tft.setTextColor(ST77XX_RED);
    tft.setCursor(5,24);
    tft.setTextSize(1);
    tft.println("Temperatur: ");
    tft.fillCircle(64,95,20,ST77XX_YELLOW);
    tft.drawLine(tft.width()/4, 95, 95, 95,ST77XX_YELLOW);
    tft.drawLine(tft.height()/2.5,125, 64, 66,ST77XX_YELLOW);
    tft.drawLine(tft.height()/1.8,117, 42, 73,ST77XX_YELLOW);
    tft.drawLine(tft.height()/3.8,118, 85, 73,ST77XX_YELLOW);
  }else if(temp < 25 && temp > 22) {
    tft.setTextColor(ST77XX_GREEN);
    tft.setCursor(5,24);
    tft.setTextSize(1);
    tft.println("Temperatur: ");
     //Left scope
  tft.drawLine(65,105,50,90,ST77XX_GREEN);
  tft.drawLine(50,90,40,100,ST77XX_GREEN);
  tft.drawLine(40,100,65,125,ST77XX_GREEN);

  //fill left scope
  tft.drawLine(50,91,65,106,ST77XX_GREEN);
  tft.drawLine(50,92,65,107,ST77XX_GREEN);
  tft.drawLine(50,93,65,108,ST77XX_GREEN);
  tft.drawLine(50,94,65,109,ST77XX_GREEN);
  tft.drawLine(50,95,65,110,ST77XX_GREEN);
  tft.drawLine(50,96,65,111,ST77XX_GREEN);
  tft.drawLine(50,97,65,112,ST77XX_GREEN);
  tft.drawLine(50,98,65,113,ST77XX_GREEN);
  tft.drawLine(50,99,65,114,ST77XX_GREEN);
  tft.drawLine(50,100,65,115,ST77XX_GREEN);
  tft.drawLine(49,100,65,116,ST77XX_GREEN);
  tft.drawLine(48,100,65,117,ST77XX_GREEN);
  tft.drawLine(47,100,65,118,ST77XX_GREEN);
  tft.drawLine(46,100,65,119,ST77XX_GREEN);
  tft.drawLine(45,100,65,120,ST77XX_GREEN);
  tft.drawLine(44,100,65,121,ST77XX_GREEN);
  tft.drawLine(43,100,65,122,ST77XX_GREEN);
  tft.drawLine(42,100,65,123,ST77XX_GREEN);
  tft.drawLine(41,100,65,124,ST77XX_GREEN);

  tft.drawPixel(49,92,ST77XX_GREEN);
  tft.drawPixel(48,93,ST77XX_GREEN);
  tft.drawPixel(47,94,ST77XX_GREEN);
  tft.drawPixel(46,95,ST77XX_GREEN);
  tft.drawPixel(45,96,ST77XX_GREEN);
  tft.drawPixel(44,97,ST77XX_GREEN);
  tft.drawPixel(43,98,ST77XX_GREEN);
  tft.drawPixel(42,99,ST77XX_GREEN);
  tft.drawPixel(41,100,ST77XX_GREEN);

  tft.drawPixel(49,93,ST77XX_GREEN);
  tft.drawPixel(48,94,ST77XX_GREEN);
  tft.drawPixel(47,95,ST77XX_GREEN);
  tft.drawPixel(46,96,ST77XX_GREEN);
  tft.drawPixel(45,97,ST77XX_GREEN);
  tft.drawPixel(44,98,ST77XX_GREEN);
  tft.drawPixel(43,99,ST77XX_GREEN);
  tft.drawPixel(42,100,ST77XX_GREEN);

  tft.drawPixel(49,94,ST77XX_GREEN);
  tft.drawPixel(48,95,ST77XX_GREEN);
  tft.drawPixel(47,96,ST77XX_GREEN);
  tft.drawPixel(46,97,ST77XX_GREEN);
  tft.drawPixel(45,98,ST77XX_GREEN);
  tft.drawPixel(44,99,ST77XX_GREEN);
  tft.drawPixel(43,100,ST77XX_GREEN);

  tft.drawPixel(49,95,ST77XX_GREEN);
  tft.drawPixel(48,96,ST77XX_GREEN);
  tft.drawPixel(47,97,ST77XX_GREEN);
  tft.drawPixel(46,98,ST77XX_GREEN);
  tft.drawPixel(45,99,ST77XX_GREEN);
  tft.drawPixel(44,100,ST77XX_GREEN);

  tft.drawPixel(49,96,ST77XX_GREEN);
  tft.drawPixel(48,97,ST77XX_GREEN);
  tft.drawPixel(47,98,ST77XX_GREEN);
  tft.drawPixel(46,99,ST77XX_GREEN);
  tft.drawPixel(45,100,ST77XX_GREEN);

  tft.drawPixel(49,97,ST77XX_GREEN);
  tft.drawPixel(48,98,ST77XX_GREEN);
  tft.drawPixel(47,99,ST77XX_GREEN);
  tft.drawPixel(46,100,ST77XX_GREEN);

  tft.drawPixel(49,98,ST77XX_GREEN);
  tft.drawPixel(48,99,ST77XX_GREEN);
  tft.drawPixel(47,100,ST77XX_GREEN);

  tft.drawPixel(49,99,ST77XX_GREEN);
  
  
  //Right scope
  tft.drawLine(100,70,110,80,ST77XX_GREEN);
  tft.drawLine(100,70,65,105,ST77XX_GREEN);
  tft.drawLine(110,80,65,125,ST77XX_GREEN);

  //fill right scope
  tft.drawLine(100,71,65,106,ST77XX_GREEN);
  tft.drawLine(100,72,65,107,ST77XX_GREEN);
  tft.drawLine(100,73,65,108,ST77XX_GREEN);
  tft.drawLine(100,74,65,109,ST77XX_GREEN);
  tft.drawLine(100,75,65,110,ST77XX_GREEN);
  tft.drawLine(100,76,65,111,ST77XX_GREEN);
  tft.drawLine(100,77,65,112,ST77XX_GREEN);
  tft.drawLine(100,78,65,113,ST77XX_GREEN);
  tft.drawLine(100,79,65,114,ST77XX_GREEN);
  tft.drawLine(100,80,65,115,ST77XX_GREEN);
  tft.drawLine(101,80,65,116,ST77XX_GREEN);
  tft.drawLine(102,80,65,117,ST77XX_GREEN);
  tft.drawLine(103,80,65,118,ST77XX_GREEN);
  tft.drawLine(104,80,65,119,ST77XX_GREEN);
  tft.drawLine(105,80,65,120,ST77XX_GREEN);
  tft.drawLine(106,80,65,121,ST77XX_GREEN);
  tft.drawLine(107,80,65,122,ST77XX_GREEN);
  tft.drawLine(108,80,65,123,ST77XX_GREEN);
  tft.drawLine(109,80,65,124,ST77XX_GREEN);

  tft.drawPixel(101,72,ST77XX_GREEN);
  tft.drawPixel(102,73,ST77XX_GREEN);
  tft.drawPixel(103,74,ST77XX_GREEN);
  tft.drawPixel(104,75,ST77XX_GREEN);
  tft.drawPixel(105,76,ST77XX_GREEN);
  tft.drawPixel(106,77,ST77XX_GREEN);
  tft.drawPixel(107,78,ST77XX_GREEN);
  tft.drawPixel(108,79,ST77XX_GREEN);
  tft.drawPixel(109,80,ST77XX_GREEN);

  tft.drawPixel(101,73,ST77XX_GREEN);
  tft.drawPixel(102,74,ST77XX_GREEN);
  tft.drawPixel(103,75,ST77XX_GREEN);
  tft.drawPixel(104,76,ST77XX_GREEN);
  tft.drawPixel(105,77,ST77XX_GREEN);
  tft.drawPixel(106,78,ST77XX_GREEN);
  tft.drawPixel(107,79,ST77XX_GREEN);
  tft.drawPixel(108,80,ST77XX_GREEN);
  

  tft.drawPixel(101,74,ST77XX_GREEN);
  tft.drawPixel(102,75,ST77XX_GREEN);
  tft.drawPixel(103,76,ST77XX_GREEN);
  tft.drawPixel(104,77,ST77XX_GREEN);
  tft.drawPixel(105,78,ST77XX_GREEN);
  tft.drawPixel(106,79,ST77XX_GREEN);
  tft.drawPixel(107,80,ST77XX_GREEN);
  

  tft.drawPixel(101,75,ST77XX_GREEN);
  tft.drawPixel(102,76,ST77XX_GREEN);
  tft.drawPixel(103,77,ST77XX_GREEN);
  tft.drawPixel(104,78,ST77XX_GREEN);
  tft.drawPixel(105,79,ST77XX_GREEN);
  tft.drawPixel(106,80,ST77XX_GREEN);

  tft.drawPixel(101,76,ST77XX_GREEN);
  tft.drawPixel(102,77,ST77XX_GREEN);
  tft.drawPixel(103,78,ST77XX_GREEN);
  tft.drawPixel(104,79,ST77XX_GREEN);
  tft.drawPixel(105,80,ST77XX_GREEN);

  tft.drawPixel(101,77,ST77XX_GREEN);
  tft.drawPixel(102,78,ST77XX_GREEN);
  tft.drawPixel(103,79,ST77XX_GREEN);
  tft.drawPixel(104,80,ST77XX_GREEN);

  tft.drawPixel(101,78,ST77XX_GREEN);
  tft.drawPixel(102,79,ST77XX_GREEN);
  tft.drawPixel(103,80,ST77XX_GREEN);

  tft.drawPixel(101,79,ST77XX_GREEN);
  }
  tft.setTextColor(ST77XX_WHITE);
  tft.setCursor(75,24);
  tft.setTextSize(1);
  tft.println(temp);
  tft.setTextColor(ST77XX_WHITE);
  tft.setCursor(106,24);
  tft.setTextSize(1);
  tft.println(" C");
  tft.drawCircle(107,25,2,ST77XX_WHITE);

  //HUMI text
  tft.setTextColor(ST77XX_GREEN);
  tft.setCursor(5,34);
  tft.setTextSize(1);
  tft.println("Humidity: ");
  tft.setTextColor(ST77XX_WHITE);
  tft.setCursor(75,34);
  tft.setTextSize(1);
  tft.println(humi);
  tft.setCursor(100,34);
  tft.setTextSize(1);
  tft.println(" %");

  //PRES text
  tft.setTextColor(ST77XX_CYAN);
  tft.setCursor(5,44);
  tft.setTextSize(1);
  tft.println("Pressure: ");
  tft.setTextColor(ST77XX_WHITE);
  tft.setCursor(75,44);
  tft.setTextSize(1);
  tft.println(pres / 100);
  tft.setCursor(105,44);
  tft.setTextSize(1);
  tft.println(" Pa");

  //ALTI text
  tft.setTextColor(ST77XX_COLMOD);
  tft.setCursor(5,54);
  tft.setTextSize(1);
  tft.println("Altitude: ");
  tft.setTextColor(ST77XX_WHITE);
  tft.setCursor(75,54);
  tft.setTextSize(1);
  tft.println(alti);
  tft.setCursor(105,54);
  tft.setTextSize(1);
  tft.println(" m");
  tft.drawLine(tft.width()-1, 64, 0, 64,ST77XX_WHITE);
  
  //LED-Level
  if(bme.readHumidity() < 40) {
    tft.setCursor(30,152);
    tft.setTextColor(ST77XX_WHITE);
    tft.println("LED Stufe: 1");
  }else if(bme.readHumidity() > 40 && bme.readHumidity() < 50) {
    tft.setCursor(30,152);
    tft.setTextColor(ST77XX_WHITE);
    tft.println("LED Stufe: 2");
  }else if(bme.readHumidity() > 50) {
    tft.setCursor(30,152);
    tft.setTextColor(ST77XX_WHITE);
    tft.println("LED Stufe: 3");
}

 //LED-Farbe
 if(temp < 22) {
    for (size_t i = 0; i < 18; i++)
  {
   strip.setPixelColor(i,strip.Color(0,0,255)); 
   delay(100);
  }
  strip.show();
  }else if(temp > 25) {
    for (size_t i = 0; i < 18; i++)
  {
   strip.setPixelColor(i,strip.Color(0,255,0)); 

  }
  strip.show();
  }else if(temp < 25 & temp > 22) {
     for (size_t i = 0; i < 18; i++)
  {
   strip.setPixelColor(i,strip.Color(255,0,0)); 
   //strip.setBrightness(200);
  }
  strip.show();
  }
  //LED-Helligkeit
  if(bme.readHumidity() < 40) {
    strip.setBrightness(50);
  }else if(bme.readHumidity() > 40 && bme.readHumidity() < 50) {
    strip.setBrightness(150);
  }else if(bme.readHumidity() > 50) {
    strip.setBrightness(250);
}

}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message:");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();

  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  
  }
}

void reconnect() {
  
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    
    String clientId = "SchuelerClient-";
    clientId += String(random(0xffff), HEX);
   
    if (client.connect(clientId.c_str(), ssid, password)) {
      Serial.println("connected");
    
      client.subscribe(IN_TOPIC);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      delay(5000);
    }
  }
}

void setup() {

  //Clock
  Serial.begin(9600);
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  timeClient.begin();

  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);

  //NEOPIXEL
  strip.begin();
  strip.setBrightness(50);
  strip.show(); // Initialize all pixels to 'off'
  
  //Display
  Serial.print(F("Hello! ST77xx TFT Test")); 
  tft.initR(INITR_BLACKTAB);      // Init ST7735S chip, black tab 
  Serial.println(F("Initialized")); 
  uint16_t time = millis(); 
  tft.fillScreen(ST77XX_BLACK); 
  time = millis() - time;
  Serial.println(time, DEC); 
  delay(500); 

  //BME
  Serial.println(F("BME280 test"));
  unsigned status;
  status = bme.begin(0x76);

  if (!status)
  {
    Serial.println("Could not find a valid BME280 sensor, check wiring, address, sensor ID!");
    Serial.print("SensorID was: 0x");
    Serial.println(bme.sensorID(), 16);
    while (1)
      delay(10);
  }

}

void loop() {

   if (!client.connected()) {
    reconnect();
  }
  client.loop();

  unsigned long now = millis();
  if (now - lastMsg > 5000) {
    lastMsg = now;
    ++value;
    snprintf (msg, MSG_BUFFER_SIZE, "%f", bme.readTemperature());
    client.publish(OUT_TOPIC, msg); 

    snprintf (msg, MSG_BUFFER_SIZE, "%f", bme.readHumidity());
    client.publish(OUT_TOPIC1, msg); 
  }

  tftPrintTextAndRead();
  delay(1000);
  
}


